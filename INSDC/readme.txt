1. The folder name is the scientific organism;

2. The txt file under the folder contains the merged groups collected for that organism. The detailed collection procedure is in the paper;

3. For each row in the txt file, its elements are space separated: the first is the id for exemeplar; the last is the possible merge date;
and the middle is the id(s) for duplicates which was replaced by the exemeplar

4. Let's look at a specific example from the first line of Arabidopsis thaliana file: 

342160304	AF140058.1	AF140059.1	Jul 29, 2011 04:13 PM

The first is exemeplar record, gi:342160304 (https://www.ncbi.nlm.nih.gov/nuccore/342160304);
The middle is the merged record ids, accession:AF140058.1 (https://www.ncbi.nlm.nih.gov/nuccore/AF140058.1?report=genbank) 
and accession: AF140059.1 (https://www.ncbi.nlm.nih.gov/nuccore/AF140059.1?report=genbank).
The date shows record gi:342160304 replaced the other two records possibly before 29th July 2011.

