1. The folder name is the scientific organism;

2. The txt file under the folder contains the mapping (between INSDC and TrEMBL) cases where duplicates occur

3. For each row, the format is:
Protein record accession: cross-referenced nucleotide record 1, cross-referenced nucleotide record 2...

4. Let's look at an example from the first line of Arabidopsis Thaliana file:
Q45GI1:DQ132721 CP002688 

The first is TrEMBL protein record with accession Q45GI1 (http://www.uniprot.org/uniprot/Q45GI1).
The rest two records are INSDC nucleotide records such as record with accession DQ132721 
(https://www.ncbi.nlm.nih.gov/nuccore/DQ132721). Those records point to the same protein record
and have been explicitly cross-referenced via automatic curation.

When developing duplicate detection or record linkage method, it should be tested without giving the mapping, 
i.e., without providing the protein record. For instance, compare nucleotide records pairwise in terms of meta
data and sequence similarity. The benchmarks should be used for duplicate detection within nucleotide databases.